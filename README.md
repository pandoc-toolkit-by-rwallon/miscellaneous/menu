# Pandoc Template for a *Menu*

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/menu/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/menu/commits/master)

## Description

This project provides a template allowing an easy creation of menus with
[Pandoc](https://pandoc.org).

This template is distributed under a Creative Commons Attribution 4.0
International License.

[![CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)

## Requirements

To build your menu using this template, [Pandoc](https://pandoc.org) at least
2.0 have to be installed on your computer.

## Creating your Menu

To create your menu, you need to setup its YAML configuration.
For convenience, we provide a [`metadata.md`](./metadata.md) file in which all
possible settings are specified, so that you can just set them or remove the
ones which do not fit your needs.
Read below for details on how to configure your menu, and see our various
[examples](examples).

# Informations about the Meal

You may specify the `name` and `date` of your meal by setting these variables
accordingly.

The lists `courses` and `beverages` allow to specify the content of your meal.
The latter is optional.
Note that, if you are writing these informations in a language that is not
English, we advise you to set the variable `language` appropriately.

### Images

You can customize the images appearing on the front, inside and back of the
menu by setting `front-image`, `inside-image` and `back-image` to the images
to use, respectively.
Note that `inside-image` is optional, but the others are required.

You may also specify the `ornament-image`, which is used as a separator for
the courses appearing in the menu.
A default ornament is provided if this variable is not set.

### Colors

#### Color Definitions

You may define your own colors through the variable `color`, which must be
set to a list of objects defining three fields:

+ `name`: the name of the color you are defining.
+ `type`: the type of the color, among `gray`, `rgb`, `RGB`, `html`, `HTML`,
  etc.
+ `value`: the actual value of the color.

Note that each color is then automatically translated into LaTeX by using the
following command:

```latex
\definecolor{name}{type}{value}
```

#### Color Settings

Once you have defined your own colors, you may set the following properties
to customize the colors of your menu (of course, you may also use predefined
colors):

+ `title-color`: the color for the text on the title page of the menu.
+ `course-color`: the color for the courses and beverages.
+ `ornament-color`: the color for the native ornament (ignored if a custom
  ornament is provided).

By default, all these colors are set to `black`.

## Building your Menu

Suppose that you have written the content of your menu in a file `input.md`.
Then, to produce the menu in a file named `output.pdf`, execute the following
command:

```bash
pandoc --template menu.pandoc input.md -o output.pdf
```
