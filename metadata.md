---
# LaTeX Configuration.
language:

# Color Definitions.
color:
    - name:
      type:
      value:

# Color Settings.
title-color:
course-color:
ornament-color:

# Images.
front-image:
inside-image:
ornament-image:
back-image:

# Heights
ornament-height:
ornament-code:
back-height:

# Informations about the Meal.
name:
date:

# Content of the Menu.
courses:
    -
beverages:
    -
---
