# Changelog

This file describes the evolution of the *Menu* LaTeX template for Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.2.0 (January 2022)

+ Provides more flexibility for changing the height of the images used in
  the menu.
+ Allows to use different native ornaments rather than only the default one.

## Version 0.1.0 (December 2019)

+ The name and date of the meal can be set from YAML.
+ The list of courses and beverages can be set from YAML.
+ Images can be set from YAML.
+ A custom ornament can be used to separate the different courses.
+ Colors can be customized from YAML.
