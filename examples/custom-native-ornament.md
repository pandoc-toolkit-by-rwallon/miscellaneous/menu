---
# Color Settings.
title-color: JungleGreen
course-color: Turquoise

# Images.
front-image: images/markdown.png
ornament-image: images/ornament.png
back-image: images/latex.png

# Informations about the Meal.
name: Release 0.2.0
date: January 1st, 2022

# Content of the Menu.
courses:
    - Smoked Salmon Carpaccio
    - Rich Seafood Chowder
    - Pork Medallions in Mustard Sauce
    - Stollen Butter Rolls
beverages:
    - Water
    - Coke
    - Iced Tea
    - Apple Juice
---
