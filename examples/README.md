# Use Cases of the *Menu* Template

This directory contains examples illustrating different use cases of the
*Menu* Pandoc template.

For each of the examples, you may either read its metadata file (to see how we
configured it) or download the final PDF.

## Native Ornament

This menu uses the native ornament provided by the template to separate
the different courses, with a custom color.

*Metadata file available [here](native-ornament.md).*

*PDF available [here](/../builds/artifacts/master/file/native-ornament.pdf?job=make-examples).*

## Custom Native Ornament

This menu uses a native ornament that has been  to separate
the different courses, with a custom color.

*Metadata file available [here](native-ornament.md).*

*PDF available [here](/../builds/artifacts/master/file/native-ornament.pdf?job=make-examples).*

## Custom Ornament

This menu uses a custom ornament to separate the different courses, and does
not have any image on the inside.

This example also declares the beverages available during the meal.

*Metadata file available [here](custom-ornament.md).*

*PDF available [here](/../builds/artifacts/master/file/custom-ornament.pdf?job=make-examples).*
